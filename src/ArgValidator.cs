using System;
using System.Reflection;
using System.Text.RegularExpressions;


namespace ArgValidator
{
  public static class ArgValidator
  {
    public static void IsNotNull(string argName, object argValue)
    {
      if (argValue == null) {
        throw new ArgumentNullException(argName);
      }
    }


    public static void IsValid<T>(string argName, T argValue, Func<T, bool> validationFunc)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);
      ArgValidator.IsNotNull(nameof(validationFunc), validationFunc);

      if (!validationFunc(argValue)) {
        throw new ArgumentException(argName);
      }
    }


    public static void IsTrue(string argName, bool condition, string failureMsg = null)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (!condition) {
        if (failureMsg != null) {
          throw new ArgumentException(failureMsg, argName);
        }
        throw new ArgumentException(argName);
      }
    }


    public static void IsFalse(string argName, bool condition, string failureMsg = null)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (condition) {
        if (failureMsg != null) {
          throw new ArgumentException(failureMsg, argName);
        }
        throw new ArgumentException(argName);
      }
    }


    public static void AreEqual(string argName, object argValue, object expectedValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if ((argValue == null && expectedValue != null) || !argValue.Equals(expectedValue)) {
        throw new ArgumentException($"Expected `{expectedValue}` but was given `{argValue}`", argName);
      }
    }


    public static void AreNotEqual(string argName, object argValue, object unexpectedValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if ((argValue == null && unexpectedValue == null) || argValue.Equals(unexpectedValue)) {
        throw new ArgumentException($"Value cannot equal `{unexpectedValue}`", argName);
      }
    }


    public static void AreSame(string argName, object argValue, object expectedValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (argValue != expectedValue) {
        throw new ArgumentException($"Expected reference to `{expectedValue}` but was given `{argValue}`", argName);
      }
    }


    public static void AreNotSame(string argName, object argValue, object unexpectedValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (argValue != unexpectedValue) {
        throw new ArgumentException($"Value cannot be a reference to `{unexpectedValue}`", argName);
      }
    }


    #region IComparable

    public static void IsLessThan<T, U>(string argName, T argValue, U thresholdValue)
      where T : IComparable
      where U : IComparable
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if ((argValue == null && thresholdValue == null)
          || (argValue != null && argValue.CompareTo(thresholdValue) >= 0)
          || thresholdValue.CompareTo(argValue) < 0) {
        throw new ArgumentOutOfRangeException(argName, argValue, $"Expected value to be less than {thresholdValue}");
      }
    }


    public static void IsLessThanOrEqualTo<T, U>(string argName, T argValue, U thresholdValue)
      where T : IComparable
      where U : IComparable
    {
      ArgValidator.IsNotNull(nameof(argName), argName);


      if (argValue == null && thresholdValue == null) {
        return;
      }

      if ((argValue != null && argValue.CompareTo(thresholdValue) > 0)
          || thresholdValue.CompareTo(argValue) <= 0) {
        throw new ArgumentOutOfRangeException(argName, argValue, $"Expected value to be less than or equal to {thresholdValue}");
      }
    }


    public static void IsGreaterThan<T, U>(string argName, T argValue, U thresholdValue)
      where T : IComparable
      where U : IComparable
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if ((argValue == null && thresholdValue == null)
          || (argValue != null && argValue.CompareTo(thresholdValue) <= 0)
          || thresholdValue.CompareTo(argValue) > 0) {
        throw new ArgumentOutOfRangeException(argName, argValue, $"Expected value to be less than {thresholdValue}");
      }
    }


    public static void IsGreaterThanOrEqualTo<T, U>(string argName, T argValue, U thresholdValue)
      where T : IComparable
      where U : IComparable
    {
      ArgValidator.IsNotNull(nameof(argName), argName);


      if (argValue == null && thresholdValue == null) {
        return;
      }

      if ((argValue != null && argValue.CompareTo(thresholdValue) < 0)
          || thresholdValue.CompareTo(argValue) >= 0) {
        throw new ArgumentOutOfRangeException(argName, argValue, $"Expected value to be greater than or equal to {thresholdValue}");
      }
    }

    #endregion


    #region String

    public static void IsEmptyString(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (argValue?.Length != 0) {
        throw new ArgumentException($"Expected a non-empty string but was given `{argValue}`", argName);
      }
    }


    public static void IsNotEmptyString(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (argValue?.Length == 0) {
        throw new ArgumentException($"Expected an empty string but was given `{argValue}`", argName);
      }
    }


    public static void IsWhiteSpace(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (!String.IsNullOrEmpty(argValue) && argValue.Trim().Length == 0) {
        throw new ArgumentException($"Expected an empty string or a string with only white space but was given `{argValue}`", argName);
      }
    }


    public static void IsNotWhiteSpace(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (String.IsNullOrEmpty(argValue) || argValue.Trim().Length != 0) {
        throw new ArgumentException($"Expected a non-whitespace, non-empty string but was given `{argValue}`", argName);
      }
    }


    public static void IsNullOrEmpty(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (!String.IsNullOrEmpty(argValue)) {
        throw new ArgumentException($"Expected `null` or an empty string but was given `{argValue}`", argName);
      }
    }


    public static void IsNotNullOrEmpty(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (String.IsNullOrEmpty(argValue)) {
        throw new ArgumentException($"Expected a non-null, non-empty string but was given `{argValue}`", argName);
      }
    }


    public static void IsNullOrWhiteSpace(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (!String.IsNullOrWhiteSpace(argValue)) {
        throw new ArgumentException($"Expected `null`, an empty string, or a white space only string but was given `{argValue}`", argName);
      }
    }


    public static void IsNotNullOrWhiteSpace(string argName, string argValue)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (String.IsNullOrEmpty(argValue)) {
        throw new ArgumentException($"Expected a non-null, non-empty, non-white-space string but was given `{argValue}`", argName);
      }
    }


    public static void IsMatch(string argName, string argValue, string pattern, RegexOptions regexOptions = RegexOptions.None)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);
      ArgValidator.IsNotNull(nameof(pattern), pattern);

      if (!Regex.IsMatch(argValue, pattern, regexOptions)) {
        throw new ArgumentOutOfRangeException(argName, argValue, $"Expected match with `{pattern}` (with the following options: `{regexOptions}`)");
      }
    }


    public static void IsNotMatch(string argName, string argValue, string pattern, RegexOptions regexOptions = RegexOptions.None)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);
      ArgValidator.IsNotNull(nameof(pattern), pattern);

      if (Regex.IsMatch(argValue, pattern, regexOptions)) {
        throw new ArgumentOutOfRangeException(argName, argValue, $"Value cannot match with `{pattern}` (with the following options: `{regexOptions}`)");
      }
    }

    #endregion


    #region Type

    public static void IsEnum(string argName, Type argType)
    {
      ArgValidator.IsNotNull(nameof(argName), argName);

      if (!argType.GetTypeInfo().IsEnum) {
        throw new ArgumentException($"Expected type to be an Enum but was given {argType.FullName}", argName);
      }
    }

    #endregion
  }
}
